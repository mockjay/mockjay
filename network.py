# -*- coding: utf-8 -*-
"""
Created on Sat Sep 17 10:48:23 2016

@author: ahmad
"""
import random
import json

if __name__ == '__main__':

    nodes = []
    links = []
    
    num_nodes = 20
    num_links = 50
    for iNode in range(num_nodes):
        node = dict()
        node["id"] = iNode    
        node["group"] = iNode % 4
        nodes.append(node)
        
    for iLink in range(num_links):
        link = dict()
        link["source"] = nodes[random.randrange(1, num_nodes)]["id"]  
        link["target"] = nodes[random.randrange(1, num_nodes)]["id"]
        link["value"] = random.randrange(1,5)
        links.append(link)
        
    print  json.dumps({"nodes":nodes, "links":links})
