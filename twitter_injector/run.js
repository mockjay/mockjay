var Twitter = require('twitter')
var Peer = require('simple-peer')
var wrtc = require('wrtc')
var ws = require('ws')

var hostname = process.argv[2]
console.log("Connecting to " + hostname)

var websocket = new ws('ws://' + hostname + ':8888')

var peers = {}

function guidGenerator() {
  var S4 = function() {
     return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
  };
  return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

function connectPeer(destinationId, initializer) {
    var p = new Peer({ initiator: initializer, trickle: false, wrtc: wrtc })
    peers[destinationId] = p
    p.on('error', function (err) { console.log('error', err) })
    p.on('signal', (data) => {
      console.log('SIGNAL', JSON.stringify(data))
      console.log("signal from " + destinationId)
      websocket.send(JSON.stringify({
        sourceId: ourId,
        destinationId: destinationId,
        type: "signal",
        message: JSON.stringify(data)
      }))
    })

    p.on('connect', function () {
      console.log('CONNECT')
    })

    p.on('data', (data) => {
      console.log("Received data")
      console.log(data)
    })

    p.on('close', () => {
      delete peers[destinationId]
    })

    p.on('error', (err) => {
      console.error(err)
    })

    return p;
  }

var ourId = guidGenerator()

console.log("We are " + ourId)

websocket.onopen = () => {
  console.log("websocket open")
  websocket.send(JSON.stringify({
    sourceId: ourId,
    type: "register"
  }))
}

websocket.onmessage = (data) => {
  var msg = JSON.parse(data.data)
  var sourceId = msg.sourceId
  if (msg.type == "register" && msg.sourceId !== ourId) {
    var p = connectPeer(sourceId, true)
  }
  if (msg.type == "signal") {
    console.log("Got signal")
    var signal = JSON.parse(msg.message)
    if (!(sourceId in peers)) {
      p = connectPeer(sourceId, false)
      console.log("Setting signal")
    }
    p = peers[sourceId]
    p.signal(signal)
  }
}

var client = new Twitter({
  consumer_key: 'hSsAOuidMPms6U57accQioaWT',
  consumer_secret: 'R1FZgXBmRY7qcIoBc3ULxxQglp4vKMvDCNWBKA9Kw1bpEOeuG3',
  access_token_key: '403774321-V0RjBy90ZYFekT76PBmjmKO1gEBjd2Kj5B7TPTlP',
  access_token_secret: 'aufo2k84XDvh9qItnoAIT0ermmSw5njPbSaQ9am2hdSYB'
});

client.stream('statuses/sample', function(stream) {
  stream.on('data', function(tweet) {
    if (tweet.text) {
      for(peerId in peers) {
        var peer = peers[peerId]
        peer.send(JSON.stringify({
          username: tweet.user.screen_name,
          text: tweet.text,
          twitter: true
        }))
      }
    }
  });
  stream.on('error', function(error) {
    console.log(error)
  });
})
