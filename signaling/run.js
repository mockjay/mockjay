var WebSocketServer = require('ws').Server;

var wss = new WebSocketServer({port: 8888});

var connectedClients = {}

wss.broadcast = function(data) {
    console.log("Broadcasting " + data)
    for(var i in this.clients) {
        this.clients[i].send(data);
    }
};

wss.on('connection', function(ws) {
    console.log("Node connected")
    ws.on('message', function(message) {
        // Register message
        jsonMsg = JSON.parse(message)
        if(jsonMsg.type && jsonMsg.type == "register" && jsonMsg.sourceId) {
            console.log("Node " + jsonMsg.sourceId + "registered")
            // Store the websocket for each node
            connectedClients[jsonMsg.sourceId] = ws
            wss.broadcast(message);
        }
        if(jsonMsg.destinationId) {
            console.log("Forwarding message from " + jsonMsg.sourceId + " to " + jsonMsg.destinationId)
            let dst = connectedClients[jsonMsg.destinationId]
            dst.send(message)
        }
    });
});

console.log("Signaling server is running...")
