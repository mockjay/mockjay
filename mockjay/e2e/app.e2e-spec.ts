import { MockjayPage } from './app.po';

describe('mockjay App', function() {
  let page: MockjayPage;

  beforeEach(() => {
    page = new MockjayPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
