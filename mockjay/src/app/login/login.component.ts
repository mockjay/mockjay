import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor() { 

  }

  name: string

  handleKeypress(event: KeyboardEvent){
    if (event.key === "Enter"){
      console.log("Enter pressed!")
      location.replace("app/" + this.name)
    }
  }


  ngOnInit() {
  }

}
