import Peer = require('./Peer.ts')
import RemotePeer = require('./RemotePeer.ts')

class LocalPeer extends Peer {
  id: string
  peers: { [id: string] : RemotePeer; }
  rtcpeerconns: { [id: string] : RTCPeerConnection; }
  rtcdatachannels: { [id: string] : RTCDataChannel; }
  websocket:WebSocket
  websocketConnected:boolean
  onWebsocketConnectedCallback:Function
  onNewPeer:Function
  onPeerConnected:Function
  onPeerDisconnected:Function
  onRTCMessage:Function = null
  constructor(onWebsocketConnectedCallback, onNewPeer) {
    super()
    // Websocket stuff
    this.onWebsocketConnectedCallback = onWebsocketConnectedCallback
    this.onNewPeer = onNewPeer
    // Websocket
    var protocol = location.protocol === 'https:' ? 'wss://' : 'ws://'
    this.websocket = new WebSocket(protocol + window.location.hostname + '/websocket')
    // When the socket connection opens
    this.websocket.onopen = () => {
      this.websocketConnected = true
      this.onWebsocketConnectedCallback()
    }
    // When the socket connection closes
    this.websocket.onclose = () => {
      this.websocketConnected = false
    }
    // Handle websocket
    this.websocket.onmessage = (input) => {
      var message = JSON.parse(input.data);
      if(message.type && message.type === 'offer') {
        this.handleOffer(message)
      } else if(message.type && message.type === 'answer') {  
        this.handleAnswer(message)
      } else if(message.type && message.type === 'peers') {
        this.handlePeers(message)
      }
      else if(message.type && message.type === 'icecandidate') {
        this.handleIceCandidate(message)
      } else if(message.type && message.type === 'peer_disconnect') {
        this.handleDisconnect(message)
      }
      else {
        console.log("Received unknown message")
        console.log(message)
      }
    };
    this.id = this.guidGenerator()
    this.rtcpeerconns = {}
    this.rtcdatachannels = {}
    this.peers = {}
  }

  handleOffer(message) {
    var peerId = message.sourceId;
    var peer = new RemotePeer(peerId)
    this.addPeer(peer)
    this.prepareConnection(peer);
    console.log(message.message)
    var offer = new RTCSessionDescription(message.message);
    this.rtcpeerconns[peerId].setRemoteDescription(offer, () => {
        this.rtcpeerconns[peerId].createAnswer((answer) => {
            this.rtcpeerconns[peerId].setLocalDescription(answer, () => {
                var output = answer;
                this.websocket.send(JSON.stringify({
                    inst: 'send',
                    sourceId: this.id,
                    peerId: peerId,
                    message: output,
                    type: 'answer'
                }));
            }, this.onError);
        }, this.onError);                
    }, this.onError);
  }

  handleAnswer(message) {
    console.log("handleAnswer")
    var answer = new RTCSessionDescription(message.message);
    this.rtcpeerconns[message.sourceId].setRemoteDescription(
      answer, function() {/* handler required but we have nothing to do */},
      this.onError
    );
  }

  handlePeers(message) {
    console.log("handlePeers [Got " + message.data.length + " peers]")
    var newPeers = []
    message.data.forEach((peerId) => {
      var peer = new RemotePeer(peerId)
      this.addPeer(peer)
    })
  }

  handleIceCandidate(message:any) {
    console.log("handleIceCandidate")
    console.log(message.message)
    var candidate = new RTCIceCandidate(message.message)
    // ignore ice candidates until remote description is set
    var peerId = message.peerId
    this.prepareConnection(new RemotePeer(peerId))
    console.log(message.sourceId)
    this.rtcpeerconns[message.sourceId].addIceCandidate(candidate, () => {
        console.log("Ice candidate added")
    }, (e) => {
        console.log("Error")
        console.log(e)
    })
  }

  handleDisconnect(message) {
    console.log("handleDisconnect")
    var peerId = message.data
    if(!this.rtcdatachannels.hasOwnProperty(peerId)) {
      console.log("Peer is already disconnected: " + peerId)
      return
    }
    this.rtcdatachannels[message.data].close();
    this.rtcpeerconns[message.data].close();
    delete this.rtcdatachannels[message.data]
    delete this.rtcpeerconns[message.data]
  }

  sendOffer(peer:RemotePeer, offer: RTCSessionDescription) {
    this.websocket.send(JSON.stringify({
      inst: 'send',
      sourceId: this.id,
      peerId: peer.id,
      message: offer,
      type: 'offer'
    }));
  }

  sendIceCandidate(peer: RemotePeer, candidate: RTCIceCandidate) {
    console.log("sendIceCandidate")
    console.log(candidate)
    this.websocket.send(JSON.stringify({
      inst: 'send',
      sourceId: this.id,
      peerId: peer.id,
      type: 'icecandidate',
      message: candidate
    }));
  }

  register() {
    this.websocket.send(JSON.stringify({
      inst: 'register',
      id: this.id
    }))
  }

  getPeers() {
    this.websocket.send(JSON.stringify({
      inst: 'peers'
    }));
  }

  guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
  }

  addPeer(peer:RemotePeer) {
    if (this.peers.hasOwnProperty(peer.id)) {
      return
    }
    this.peers[peer.id] = peer
    this.onNewPeer(peer)
  }

  prepareConnection(peer:RemotePeer) {
    var RTCPeerConnection = null
    if (webkitRTCPeerConnection) {
        RTCPeerConnection = webkitRTCPeerConnection
    }
    var rtcpeerconn = new RTCPeerConnection(
      {iceServers: [{ 'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}]}, 
      {optional: [{RtpDataChannels: false}]}
    )
    this.rtcpeerconns[peer.id] = rtcpeerconn

    // Register handlers
    rtcpeerconn.ondatachannel = (event: RTCDataChannelEvent) => {
      var rtcdatachannel:RTCDataChannel = event.channel
      this.rtcdatachannels[peer.id] = rtcdatachannel
      rtcdatachannel.onopen = () => {
        this.peers[peer.id].connected = true
        if(this.onPeerConnected) {
          this.onPeerConnected(peer)
        }
        rtcdatachannel.onmessage = (event) => {
          this.msgReceived(peer, event)
        }
      }
      rtcdatachannel.onerror = this.onRTCDataChannelError;
      rtcdatachannel.onclose = () => {
        this.peers[peer.id].connected = false
        if(this.onPeerDisconnected) {
          this.onPeerDisconnected(peer)
        }
      }
    };

    // Handle icecandidate
    rtcpeerconn.onicecandidate = (event:RTCIceCandidate) => {
      if (!event || !event.candidate) return;
      this.sendIceCandidate(peer, event.candidate)
    };
  }

  connect(peer:RemotePeer, hop:RemotePeer = null) {
    this.prepareConnection(peer)
    var rtcpeerconn = this.rtcpeerconns[peer.id]
    var rtcdatachannel = rtcpeerconn.createDataChannel(peer.id + this.id, {
      ordered: false,
      maxRetransmits: 5
    });
    this.rtcdatachannels[peer.id] = rtcdatachannel
    rtcdatachannel.onopen = () => {
      this.peers[peer.id].connected = true
      if (this.onPeerConnected) {
        this.onPeerConnected(peer)
      }
      rtcdatachannel.onmessage = (event:RTCMessageEvent) => {
        this.msgReceived(peer, event)
      }
    }
    rtcdatachannel.onclose = () => {
      this.peers[peer.id].connected = false
      if(this.onPeerDisconnected) {
        this.onPeerDisconnected(peer)
      }
    }
    rtcdatachannel.onerror = this.onRTCDataChannelError;

    rtcpeerconn.createOffer((offer) => {
      rtcpeerconn.setLocalDescription(offer, () => {
          this.sendOffer(peer, offer)
      }, this.onError);
    }, this.onError);
  }

  msgReceived(peer:Peer, message:RTCMessageEvent) {
      console.log(message.data)
      if(this.onRTCMessage) {
        this.onRTCMessage(peer, message)
      }
  }

  send(peer:RemotePeer, message:string):boolean {
    if(!this.rtcdatachannels.hasOwnProperty(peer.id)) {
      console.log("Cannot send message to peer " + peer.id)
      return false
    }
    var datachannel = this.rtcdatachannels[peer.id]
    if (datachannel.readyState !== "open") {
      console.log("Error, connection is not open")
      return false;
    }
    this.rtcdatachannels[peer.id].send(message)
    return true
  }

  onRTCDataChannelError() {
      console.log("RTCDataChannelError")
  }
  onRTCPeerConnectionClose() {
      console.log("RTCPeerConnectionClose")
  }
  onError(message) {
      console.log("General error occured" + message)
  }
}
export = LocalPeer
