FROM node:6.6.0-wheezy
RUN apt-get update && apt-get install -y nginx
ADD ./mockjay/dist /usr/share/nginx/www
ADD ./signaling /signaling
CMD nginx && node /signaling/run.js
